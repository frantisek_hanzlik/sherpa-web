import { ThemeProvider } from "@emotion/react";
import type { AppProps } from "next/app";
import { themes } from "../common/styles/theme";

const CustomApp = ({ Component, pageProps }: AppProps) => {
	return (
		<ThemeProvider theme={themes.light}>
			<Component {...pageProps} />
		</ThemeProvider>
	);
};
export default CustomApp;
