import { css, keyframes } from "@emotion/react";
import styled from "@emotion/styled";
import {
	computeScaledImageStyles,
	ImageSized,
} from "../../common/components/image sized";
import { pageHeight } from "../../common/components/page";
import { phoneNumber } from "../../common/config/branding";
import { imagesBranding } from "../../common/images/branding";
import { background } from "../../common/styles/colors";
import { text } from "../../common/styles/text";

const logo = imagesBranding.logoSimple;

const logoSize = computeScaledImageStyles(logo, {
	width: "calc(20rem + 12vw)",
});

export const FirstPage = () => (
	<Container id="home">
		<ScrollDownArrowSpacer />
		<CentredContent>
			<ImageSized src={logo} alt="" size={logoSize} />
			<TelephoneNumber href={`tel:${phoneNumber}`}>
				{phoneNumber.slice(1)}
			</TelephoneNumber>
		</CentredContent>
		<a href="#services">
			<ScrollDownButtonImg src="/static/images/icons/scroll-down.svg" />
		</a>
	</Container>
);

const TelephoneNumber = styled.a`
	position: relative;
	${text("normal", { size: "calc(1.5rem + 3vw)", color: "white" })}

	::before {
		content: "+";
		position: absolute;
		left: -0.8em;
	}

	::after {
		content: " ";
		position: absolute;
		left: 0;
		right: 0;
		bottom: -0.3rem;
		height: 0.2rem;
		${background("white")}

		transform: scaleX(0);
		transition: 0.5s transform;
	}

	:hover::after {
		transform: scaleX(1);
	}
`;

const Container = styled.div`
	position: relative;
	min-height: ${pageHeight};

	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: space-between;
	padding: 1rem;

	overflow: hidden;

	background: url("/static/images/misc/home cover/0.webp")
		${({ theme }) => theme.colors.foreground.primary};
	background-position: center;
	background-size: cover;
	background-repeat: no-repeat;
`;

const CentredContent = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
`;

const bounce = keyframes`
	0% {
		transform: translateY(0);
	}

	10% {
		transform: translateY(-1rem);
	}

	12% {
		transform: translateY(0);
	}

	15%, 16% {
		transform: translateY(-0.75rem);
	}

	18% {
		transform: translateY(0);
	}
`;

const scrollDownArrowSize = css`
	width: 5rem;
	height: 5rem;
`;

const ScrollDownArrowSpacer = styled.div`
	${scrollDownArrowSize}
`;

const ScrollDownButtonImg = styled.img`
	${scrollDownArrowSize}
	animation: ${bounce} 5s ease infinite;
`;
