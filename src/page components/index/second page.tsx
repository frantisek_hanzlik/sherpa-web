import styled from "@emotion/styled";
import { transparentize } from "polished";
import { pageHeight } from "../../common/components/page";
import { Space } from "../../common/components/space";
import { phoneNumber } from "../../common/config/branding";
import { background } from "../../common/styles/colors";
import { displayAfter, displayTo } from "../../common/styles/responsivity";
import { shadow, shadowInset } from "../../common/styles/shadows";
import { text } from "../../common/styles/text";

const { sin, PI } = Math;

export const SecondPage = () => {
	const content = (
		<Content>
			<Heading>
				Vysokozdvižné platformy
				<br />
				pro <i>váš</i> nápad
			</Heading>

			<Space height="10rem" />

			<ListBox>
				<ListHeading>Co umíme:</ListHeading>
				<List>
					<Item>Stavebně-montážní Práce</Item>
					<Item>Výstavby a opravy komínů</Item>
					<Item>Pokrývačské a klempířské práce</Item>
					<Item>Ořez a úprava stromů</Item>
					<Item>Tlakové mytí ve výškách</Item>
					<Item>Natěračské práce</Item>
				</List>
				<ListAfternote>... A další!</ListAfternote>
			</ListBox>

			<Space height="5rem" />

			<CallButton href={`tel:${phoneNumber}`}>{phoneNumber}</CallButton>
		</Content>
	);

	return (
		<>
			<Container id="services">
				<SimpleContentContainer>{content}</SimpleContentContainer>
				<FancyShadeContainer>
					<Shade>{content}</Shade>
				</FancyShadeContainer>
			</Container>
		</>
	);
};

const shadeSkewAngleDeg = -10;
const shadeSkewAngleRad = (shadeSkewAngleDeg / 180) * PI;

const Container = styled.div`
	height: ${pageHeight};
`;

const SimpleContentContainer = styled.div`
	height: 100%;
	${displayTo("tablet")}
	background: linear-gradient(to bottom, white, ${({ theme }) =>
		transparentize(0.8)(theme.colors.foreground.primary)});
`;

const FancyShadeContainer = styled.div`
	${displayAfter("tablet")}

	${shadowInset}

	display: flex;
	flex-direction: column;
	align-items: flex-end;

	height: 100%;

	overflow: hidden;

	/* background image */
	position: relative;
	::before {
		content: " ";
		position: absolute;
		left: 0;
		width: calc(40% - 10rem + ${-sin(shadeSkewAngleRad)} * ${pageHeight} / 2);
		height: 100%;
		background: url("/static/images/misc/home cover/1.webp")
			${({ theme }) => theme.colors.foreground.primary};
		background-size: cover;
	}
`;

const Shade = styled.div`
	position: relative;
	width: calc(10rem + 60%);
	height: 100%;

	padding-left: calc(${-sin(shadeSkewAngleRad)} * ${pageHeight} / 2);
	padding-right: 6rem;

	/* the skewed background */
	::before {
		content: " ";
		${shadow}
		inset: 0;
		position: absolute;
		transform: skewX(${shadeSkewAngleDeg}deg);
		right: calc(${sin(shadeSkewAngleRad)} * ${pageHeight} / 2);

		background-color: white;
	}
`;

const Content = styled.div`
	width: 100%;
	height: 100%;

	// required to keep the content above the shade background
	position: relative;

	padding: 1rem;

	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
`;

const Heading = styled.h1`
	text-align: center;
	${text("heading", { size: "4rem", color: "primary" })};
	text-shadow: 0.1rem 0.1rem 1rem hsla(0, 0%, 50%, 0.5);

	i {
		position: relative;
		z-index: 0;

		${text("heading", { size: "4rem", color: "secondary" })};
	}
`;

const ListBox = styled.div`
	/* width: 80%; */
`;

const ListHeading = styled.h2`
	${text("headingSmaller", { size: "2rem" })}
`;

const List = styled.ul`
	padding: 1.5rem 0;
	padding-left: 3rem;
	list-style: disc;
`;
const Item = styled.li`
	${text("normal", { size: "2rem" })}
	padding: 0.25rem 0;
`;

const ListAfternote = styled.p`
	padding-left: 2rem;
	${text("normalBold", { size: "1.75rem" })}
`;

const CallButton = styled.a`
	${background("accept")}
	${text("normalBold", { size: "2.5rem", color: "accept" })}
	padding: 1rem 2rem;
	border-radius: 3rem;

	transition: 0.1s opacity;

	:hover {
		opacity: 0.75;
	}
`;
