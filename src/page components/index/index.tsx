import styled from "@emotion/styled";
import { Page } from "../../common/components/page";
import { Space } from "../../common/components/space";
import { background } from "../../common/styles/colors";
import { shadow } from "../../common/styles/shadows";
import { FirstPage } from "./first page";
import { SecondPage } from "./second page";

const Home = () => {
	return (
		<Page title="Domov" activeSection="home">
			<FirstPage />
			<SpaceWithShadow height="4rem" />
			<SecondPage />
		</Page>
	);
};
export default Home;

const SpaceWithShadow = styled(Space)`
	/* ${shadow} */
	${background("normal")}
	position: relative;
	z-index: 1;
`;
