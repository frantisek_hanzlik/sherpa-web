declare module "*.svg" {
	// eslint-disable-next-line import/no-unresolved
	import dummyImage from "random image name.png";

	// the default NextJS type is `any` to avoid clashing with other libraries which might add type declarations for svg files.
	// since we don't use any of those libraries, we can supply the real type.
	const content: typeof dummyImage;

	// @ts-expect-error this triggers duplicate identifier because of the type declarations provided by Next
	// and there is not any way to avoid it as far as I can tell.
	export default content;
}
