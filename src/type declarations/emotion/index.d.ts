import "@emotion/react";
import { Theme as AppTheme } from "../../common/styles/theme";

declare module "@emotion/react" {
	export interface Theme extends AppTheme {}
}
