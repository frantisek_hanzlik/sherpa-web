import { Theme } from "@emotion/react";
import { CSSInterpolation } from "@emotion/serialize";

export type Interpolatable = (_props: { theme: Theme }) => CSSInterpolation;
