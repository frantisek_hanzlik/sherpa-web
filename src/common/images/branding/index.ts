import logo from "./logo.svg";
import logoSimple from "./logo_simple.svg";

export const imagesBranding = {
	logoSimple,
	logo,
};
