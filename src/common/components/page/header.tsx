import styled from "@emotion/styled";
import Link from "next/link";
import { math } from "polished";
import React, { useCallback, useRef, useState } from "react";
import { imagesBranding } from "../../images/branding";
import { background } from "../../styles/colors";
import { displayAfter, displayTo } from "../../styles/responsivity";
import { shadow } from "../../styles/shadows";
import { text } from "../../styles/text";
import { HamburgerButton } from "../hamburger button";
import { computeScaledImageStyles, ImageSized } from "../image sized";
import {
	disableBodyScroll,
	enableBodyScroll,
	clearAllBodyScrollLocks,
} from "body-scroll-lock";
import { css } from "@emotion/react";
import { useUnmountHandler } from "../../utils/hooks";

export const headerHeight = "6rem";

const navItems = new Map([
	[
		"home",
		{
			label: "Domů",
			url: "#home",
		},
	],
	[
		"services",
		{
			label: "Služby",
			url: "#services",
		},
	],
] as const);
export type NavSectionId = typeof navItems extends Map<infer K, unknown>
	? K
	: never;

const logo = imagesBranding.logo;

const logoSize = computeScaledImageStyles(logo, {
	height: math(`${headerHeight} - 1rem`),
});

export const Header = ({ activeSection }: { activeSection: NavSectionId }) => {
	const mobileLinksScrollContainerRef = useRef<HTMLDivElement>(null);
	const [mobileMenuOpen, setMobileMenuOpenState] = useState(false);
	const setMobileMenuOpen = useCallback((newValue: boolean) => {
		setMobileMenuOpenState(newValue);
		if (mobileLinksScrollContainerRef.current !== null) {
			if (newValue) {
				disableBodyScroll(mobileLinksScrollContainerRef.current);
			} else {
				enableBodyScroll(mobileLinksScrollContainerRef.current);
			}
		}
	}, []);
	useUnmountHandler(() => clearAllBodyScrollLocks());

	return (
		<>
			<Container>
				<Content>
					<ImageSized src={logo} alt="Website Logo" size={logoSize} />
					<Nav>
						<MobileNav>
							<HamburgerButton
								open={mobileMenuOpen}
								setOpen={setMobileMenuOpen}
							/>
						</MobileNav>
						<DesktopLinks>
							{[...navItems.entries()].map(([id, item]) => (
								<li key={item.label}>
									<Link href={item.url} passHref>
										<DesktopA active={activeSection === id}>
											{item.label}
										</DesktopA>
									</Link>
								</li>
							))}
						</DesktopLinks>
					</Nav>
				</Content>
				<MobileLinks open={mobileMenuOpen} ref={mobileLinksScrollContainerRef}>
					<nav>
						{[...navItems.entries()].map(([id, item]) => (
							<li key={id}>
								<Link href={item.url} passHref>
									<MobileLink onClick={() => setMobileMenuOpen(false)}>
										{item.label}
									</MobileLink>
								</Link>
							</li>
						))}
					</nav>
				</MobileLinks>
			</Container>
		</>
	);
};

const Container = styled.header`
	/*
		might there be some content in the page that needs to use higher z-index, cover it.
		this makes sure that the header shadow is always visible.
	*/
	position: relative;
	z-index: 100;

	display: flex;
	justify-content: center;
	${shadow}
	${background("normal")}
`;

const Content = styled.div`
	height: ${headerHeight};

	padding: 0 1rem;

	flex-basis: 80rem;
	flex-grow: 0;

	display: flex;
	justify-content: space-between;
	align-items: center;
`;

const DesktopLinks = styled.ul`
	${displayAfter("mobile", "flex")}
`;

const MobileNav = styled.div`
	${displayTo("mobile", "flex")}

	align-items: center;
`;

const MobileLinks = styled.div<{ open: boolean }>`
	position: fixed;

	/* index high enough that it is above all content, but low enough that it is beneath the header. */
	z-index: 50;
	top: 0;
	bottom: 0;
	left: 0;
	right: 0;

	display: flex;
	flex-direction: column;
	gap: 2rem;

	padding: 4rem;
	padding-top: calc(${headerHeight} + 4rem);

	${background("modal")}

	transition: 0.5s transform;

	${({ open }) =>
		open
			? ""
			: css`
					transform: translateX(100%);
			  `}
`;

const MobileLink = styled.a`
	${text("normal", { size: "2rem", color: "modal" })}
	text-decoration: none;
`;

const Nav = styled.nav`
	align-self: stretch;

	display: flex;
	align-items: stretch;
`;

const DesktopA = styled.a<{ active: boolean }>`
	display: flex;
	height: 100%;

	align-items: center;
	justify-content: center;

	padding: 1rem 1.5rem;
	background: linear-gradient(to bottom, transparent 50%, hsl(0, 0%, 95%) 50%);
	text-decoration: none;
	background-size: 100% 200%;
	${text("normal", { size: "1rem" })}
	${({ active }) =>
		active ? text("normalBold", { size: "1.1rem" }) : undefined}

	&:hover {
		background-position-y: 100%;
	}

	transition: background-position-y 0.2s;
`;
