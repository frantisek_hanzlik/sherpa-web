import { Global } from "@emotion/react";
import styled from "@emotion/styled";
import { ReactNode } from "react";
import { globalStyles } from "./global styles";
import { Header, headerHeight, NavSectionId } from "./header";
import Head from "next/head";

export const pageHeight = `calc(100vh - ${headerHeight})`;

export const Page = ({
	children,
	title,
	activeSection,
}: {
	children: ReactNode;
	title: string;
	activeSection: NavSectionId;
}) => (
	<>
		<Head>
			<title>{title} | Sherpa Hanzlík</title>
			<link rel="icon" href="/static/images/icons/favicon.png" />
		</Head>
		<Global styles={globalStyles} />
		<Container>
			<Header activeSection={activeSection} />
			<Main>{children}</Main>
		</Container>
	</>
);

const Container = styled.div`
	height: 100vh;
`;

const Main = styled.main`
	height: ${pageHeight};

	overflow: auto;
	scrollbar-color: ${({ theme }) =>
		`${theme.colors.scrollbar.thumb} ${theme.colors.scrollbar.track}`};
	scroll-behavior: smooth;
`;
