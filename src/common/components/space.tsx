import styled from "@emotion/styled";

export const Space = styled.div<{ width?: string; height?: string }>`
	width: ${({ width }) => width};
	height: ${({ height }) => height};
`;
