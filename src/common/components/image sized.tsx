import Image, { ImageProps } from "next/image";

export const ImageSized = ({
	src,
	alt,
	size,
	className,
}: {
	src: Exclude<ImageProps["src"], string>;
	alt: string;
	size: string;
	className?: string;
}) => {
	return (
		<div className={`root ${className}`}>
			<Image src={src} alt={alt} layout="fixed" />
			<style jsx>{`
				.root > :global(div) {
					${size}
				}
			`}</style>
		</div>
	);
};

export const computeScaledImageStyles = (
	src: Exclude<ImageProps["src"], string>,
	{
		width,
		height,
	}: {
		width?: string;
		height?: string;
	},
) => {
	const srcRefined = "default" in src ? src.default : src;
	const aspectRatio = srcRefined.width / srcRefined.height;

	return `
		width: ${
			width ?? `calc(${aspectRatio} * ${height ?? `${srcRefined.height}px`})`
		} !important;
		height: ${
			height ?? `calc(${width ?? `${srcRefined.width}px`} / ${aspectRatio})`
		} !important;
	`;
};
