import { css } from "@emotion/react";

export const shadow = css`
	box-shadow: 0 0 0.5rem 0 #000000;
`;

export const shadowInset = css`
	box-shadow: inset 0 0 0.5rem 0.05rem #000000;
`;
