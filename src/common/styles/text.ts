import { css, Theme } from "@emotion/react";
import { foreground } from "./colors";

const families = {
	normal: "'Nunito', sans-serif",
	heading: "'Roboto', sans-serif",
};

const bases = {
	normal: css`
		font-family: ${families.normal};
		font-weight: 400;
	`,
	normalBold: css`
		font-family: ${families.normal};
		font-weight: 600;
	`,
	heading: css`
		font-family: ${families.heading};
		font-weight: 700;
	`,
	headingSmaller: css`
		font-family: ${families.normal};
		font-weight: 700;
	`,
};

export const text =
	(
		base: keyof typeof bases,
		{
			color = "normal",
			size = "1rem",
		}: { color?: keyof Theme["colors"]["foreground"]; size?: string } = {},
	) =>
	({ theme }: { theme: Theme }) =>
		css`
			${bases[base]}
			${foreground(color)({ theme })};
			font-size: ${size};
		`;
