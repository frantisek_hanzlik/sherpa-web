import { css, Theme } from "@emotion/react";
import { Interpolatable } from "../utils/emotion";

export const background =
	(color: keyof Theme["colors"]["background"]): Interpolatable =>
	({ theme }) =>
		css`
			background-color: ${theme.colors.background[color]};
		`;

export const foreground =
	(color: keyof Theme["colors"]["foreground"]): Interpolatable =>
	({ theme }) =>
		css`
			color: ${theme.colors.foreground[color]};
		`;
