import { css } from "@emotion/react";
import { CssProps } from "./types";

const breakpoints = {
	tablet: "800px",
	mobile: "600px",
};
type BreakpointName = keyof typeof breakpoints;

export const breakpointTo = (name: BreakpointName) =>
	`@media (max-width: ${breakpoints[name]})`;

export const displayTo = (
	bp: BreakpointName,
	visibleDisplay: CssProps["display"] = "block",
) => css`
	display: none;
	${breakpointTo(bp)} {
		display: ${visibleDisplay};
	}
`;

export const displayAfter = (
	bp: BreakpointName,
	visibleDisplay: CssProps["display"] = "block",
) => css`
	display: ${visibleDisplay};
	${breakpointTo(bp)} {
		display: none;
	}
`;
